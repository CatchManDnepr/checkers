#include "checker.h"

int main(){
    int k = 5;
    int placeI;
    int placeJ;
    int ** checkerDesk = malloc(8*sizeof(*checkerDesk));
    for(int index = 0; index < 8; index++){
        checkerDesk[index] = malloc(8*sizeof(int));
    }
    inicilizateBoard(checkerDesk);
    while(!win(checkerDesk)){
        outputOnScreen(checkerDesk);
        do{
            printf("\nNow, black move.\n");
            inputPlace(0, checkerDesk, &placeI, &placeJ);
        }while(!move(&placeI, &placeJ, checkerDesk, checkerDesk[placeI][placeJ], canFight(checkerDesk, checkerDesk[placeI][placeJ])));
        outputOnScreen(checkerDesk);
        do{
            printf("\nNow, white move.\n");
            inputPlace(1, checkerDesk, &placeI, &placeJ);
        }while(!move(&placeI, &placeJ, checkerDesk, checkerDesk[placeI][placeJ], canFight(checkerDesk, checkerDesk[placeI][placeJ])));

        /*printf("\nNow, black move.\n");
        do{
            printf("Input row: ");
            placeI = get_int() - 1;
            printf("Input column: ");
            placeJ = get_int() - 1;
            printf("Input way(1(on left)/0(om right)): ");
            left = get_int();
        }while(!moveBlack(placeI, placeJ, checkerDesk, left) );
        outputOnScreen(checkerDesk);*/
    }
}

/*
function for check data on integer
@param string
@return true/false
*/
bool isInt(char str[30]){
	int i = 0;
	//int ia = str[0];
	while(str[i] != '\0'){
		if((int)str[i] < 48 || (int)str[i] > 57){
			return false;
		}
		i++;
	}
	return true;
}
/*
function for check exeption
@param string
@return true/false
*/
bool check(char str[30]){
    bool status = true;
	int num;
	if(!isInt(str)){
		status = false;
	} else if(!atoi(str)){
		if (str[0] != '0'){
			status = false;
		}
	} else {
		num = atoi(str);
		if(num < 0){
			status = false;
		} else if(num > 23){
			status = false;
		}
	}
	return status;
}
/*
function for input data
@params
@return true integer element
*/
int input(){
	char num[30], ch;
	int number;
	scanf("%[^\n]s", num);
	while(!check(num)){
		ch = getchar();
		printf("Retry: ");
		scanf("%[^\n]s", num);
	}
	number = atoi(num);
	ch = getchar();
	return number;
}
/*
function for output data on screen
@params matrix
@return
*/
void outputOnScreen(int ** mass){
/*    printf(""); */
    printf(" ");
    for(int gorizontCoordinate = 0; gorizontCoordinate < 8; gorizontCoordinate++){
        printf("  %i", gorizontCoordinate + 1);
    }
    printf("\n");
    for(int k = 0; k < 8; k++){
        printf("%i  ", k + 1);
        for(int l =0; l < 8; l++){
                printf("%i  ", mass[k][l]);
        }
        printf("\n");
    }
    printf("\n");
}
/*
function for create board with men on start position
@params
@return count minutes
*/
void inicilizateBoard(int ** checkerDesk){
    for(int i = 0; i < 8; i++){
        for(int j = 0; j < 8; j++){
            if(((i+j) % 2 != 0) && (i < 3)){
                checkerDesk[i][j] = 2;
            } else if(((i+j) % 2 != 0) && (i > 4)){
                checkerDesk[i][j] = 1;
            } else {
                checkerDesk[i][j] = 0;
            }
        }
    }
}
/*
function for check win one of them side
@params matrix
@return rezult, if true then win black or white, else if false then game continue
*/
bool win(int ** checkerDesk){
    bool status = false;
    int countBlack = 0;
    int countWhite = 0;
    for(int i = 0; i < 8; i++){
        for(int j =0; j < 8; j++){
            if (checkerDesk[i][j] % 2 == 0){
                countBlack++;
            } else if (checkerDesk[i][j] % 2 == 1){
                countWhite++;
            }
        }
    }
    if (countBlack == 0){
        printf("White side win!!!\n");
        status = true;
    } else if (countWhite == 0){
        printf("Black side win\n");
        status = true;
    }
    return status;
}
/*
function for chooce men or king
@params matrix and side
@return correct coordinates
*/
void inputPlace(int side, int ** mass, int* i, int* j){
    do{
        printf("Input row: ");
        *i = input() - 1;
        while(*i < 0 || *i > 8){
            printf("Try again: ");
            *i = input() - 1;
        }
        printf("Input column: ");
        *j = input() - 1;
        while(*j < 0 || *j > 8){
            printf("Try again: ");
            *j = input() - 1;
        }
        if((mass[(int)*i][(int)*j] % 2) != (side % 2) || mass[(int)*i][(int)*j] == 0){
            printf("Retry!\n");
        }
    }while((mass[(int)*i][(int)*j] % 2) != (side % 2) || mass[(int)*i][(int)*j] == 0);
}
/*
function for find men or king which can fight
@params matrix and side
@return true/false
*/
bool canFight(int ** mass, int side){
    int** basicMass = malloc(8*sizeof(*basicMass));
    for(int index = 0; index < 8; index++){
        basicMass[index] = malloc(8*sizeof(int));
    }
    for(int i = 0; i < 8; i++){
        for(int j =0; j < 8; j++){
            basicMass[i][j] = mass[i][j];
        }
    }
    for(int i = 0; i < 8; i++){
        for(int j =0; j < 8; j++){
            if((basicMass[i][j] % 2) == (side % 2) && basicMass[i][j] != 0){
                if(fight( &i, &j, basicMass, side)){
                    return true;
                }
            }

        }
    }
    return false;
}
/*
function for find men or king, on this place, which can fight
@params matrix and side
@return true/false
*/
bool fight(int* placeI, int* placeJ, int ** mass, int side){
    int i = (int)*placeI;
    int j = (int)*placeJ;
    bool status = false;
    if (((i + 2) < 9) && ((j + 2) < 9)){
        if(mass[i][j] % 2 == 0 || mass[i][j] == 3){
            if(((int)mass[i + 1][j + 1] % 2) != side % 2 && mass[i + 1][j + 1] != 0){
                if (mass[i + 2][j + 2] == 0){
                    mass[i + 1][j + 1] = 0;
                    mass[i + 2][j + 2] = mass[i][j];
                    mass[i][j] = 0;
                    *placeI = i + 2;
                    *placeJ = j + 2;
                    status = true;
                }
            }
        }
    }
    if (i + 2 <= 8 && j - 2 >= 0){
        if (mass[i][j] % 2 == 0 || mass[i][j] == 3){
            if((mass[i + 1][j - 1] % 2) != side % 2 && mass[i + 1][j - 1] != 0){
                if (mass[i + 2][j - 2] == 0){
                    mass[i + 1][j - 1] = 0;
                    mass[i + 2][j - 2] = mass[i][j];
                    mass[i][j] = 0;
                    *placeI = i + 2;
                    *placeJ = j - 2;
                    status = true;
                }
            }
        }

    }
    if (i - 2 >= 0 && j - 2 >= 0){
        if (mass[i][j] % 2 != 0 || mass[i][j] == 4){
            if((mass[i - 1][j - 1] % 2) != side % 2 && mass[i - 1][j - 1] != 0){
                if (mass[i - 2][j - 2] == 0){
                    mass[i - 1][j - 1] = 0;
                    mass[i - 2][j - 2] = mass[i][j];
                    mass[i][j] = 0;
                    *placeI = i - 2;
                    *placeJ = j - 2;
                    status = true;
                }
            }
        }

    }
    if (i - 2 >= 0 && j + 2 < 9){
        if (mass[i][j] % 2 != 0 || mass[i][j] == 4){
            if((mass[i - 1][j + 1] % 2) != side % 2 && mass[i - 1][j + 1] != 0){
                if (mass[i - 2][j + 2] == 0){
                    mass[i - 1][j + 1] = 0;
                    mass[i - 2][j + 2] = mass[i][j];
                    mass[i][j] = 0;
                    *placeI = i - 2;
                    *placeJ = j + 2;
                    status = true;
                }
            }
        }

    }
    return status;
}
/*
function for find men or king which can move or fight
@params matrix, side, coodinate and index for definition must fight or not
@return true/false
*/
bool move(int* placeI, int* placeJ, int ** mass, int side, bool needFight){
    outputOnScreen(mass);
    int i = (int)*placeI;
    int j = (int)*placeJ;
    bool status = true;
    bool left, up = false;
    if(needFight){
        int index = 0;
        while(fight(&i, &j, mass, side)){
            index++;
        }
        if(index < 1){
            printf("You must fight!\n");
            status = false;
        } else {
            status = true;
        }
    } else {
        printf("Input way(1 - if you want go on left, 0 - if you want go on right): ");
        left = input();
        if(mass[i][j] > 2){
            printf("Input way(1 - if you want go on up, 0 - if you want go on down): ");
            up = input();
        }
        if (left){
            if(side % 2 == 0 || (side > 2 && up == false)){
                if (i + 1 >= 8 || j + 1 >= 8){
                    printf("End of board!\n");
                    status = false;
                }  else if (mass[i + 1][j + 1] == 0) {
                    mass[i + 1][j + 1] = side;
                    mass[i][j] = 0;
                } else if ((mass[i + 1][j + 1] % 2) == side % 2){
                    printf("You can`t move across your own checkers!\n");
                    status = false;
                } else if((mass[i + 1][j + 1] % 2) != side % 2){
                    printf("You can`t fight with this checker!\n");
                    status = false;
                }
            } else if((side > 2 && up == true) || side % 2 != 0) {
                if (i - 1  < 0 || j - 1 < 0){
                    printf("End of board!\n");
                    status = false;
                }  else if (mass[i - 1][j - 1] == 0) {
                    mass[i - 1][j - 1] = side;
                    mass[i][j] = 0;
                } else if ((mass[i - 1][j - 1] % 2) == side % 2){
                    printf("You can`t move across your own checkers!\n");
                    status = false;
                } else if((mass[i - 1][j + 1] % 2) != side % 2){
                    printf("You can`t fight with this checker!\n");
                    status = false;
                }
            }
        } else {
            if(side % 2 == 0 || (side > 2 && up == false)){
                if (i + 1 >= 8 || j - 1 < 0){
                    printf("End of board!\n");
                    status = false;
                }  else if (mass[i + 1][j - 1] == 0) {
                    mass[i + 1][j - 1] = side;
                    mass[i][j] = 0;
                } else if ((mass[i + 1][j - 1] % 2) == side % 2){
                    printf("You can`t move across your own checkers!\n");
                    status = false;
                } else if((mass[i + 1][j - 1] % 2) != side % 2){
                    printf("You can`t fight with this checker!\n");
                    status = false;
                }
            } else if((side > 2 && up == true) || side % 2 != 0){
                if (i - 1  < 0 || j + 1 < 0){
                    printf("End of board!\n");
                    status = false;
                }  else if (mass[i - 1][j + 1] == 0) {
                    mass[i - 1][j + 1] = side;
                    mass[i][j] = 0;
                } else if ((mass[i - 1][j + 1] % 2) == side % 2){
                    printf("You can`t move across your own checkers!\n");
                    status = false;
                } else if((mass[i - 1][j + 1] % 2) != side % 2){
                    printf("You can`t fight with this checker!\n");
                    status = false;
                }
            }
        }
    }
    if(status == false){
        printf("Retry!\n");
    } else {
        madeKing(mass);
    }
    return status;
}
/*
function for transformation from man to kind
@params matrix
@return new generation matrix
*/
void madeKing(int** mass){
    for(int i = 0; i < 8; i++){
        if(mass[0][i] == 1){
            mass[0][i] = 3;
        }
        if(mass[7][i] == 2){
            mass[7][i] = 4;
        }
    }
}
/*
function for virtual player
@params matrix
@return new matrix
*/ /*
void virtualPlayer(int** mass, int side){
    comb* variants = NULL;
    if(canFight(mass, side)){

    } else {

    }

} */
/*
function for add combination
@params old combination of matrix and matrix
@return combination of matrix
*/ /*
void push(comb* oldVariation, int** checkerBoard) {
    int countWhite = 0;
    int countBlack = 0;
    comb * variation = oldVariation;
    for(int i = 0; i < 8; i++){
        for(int j =0; j < 8; j++){
            if (checkerBoard[i][j] % 2 == 0){
                countBlack++;
            } else if (checkerBoard[i][j] % 2 == 1){
                countWhite++;
            }
        }
    }
    if(variation == NULL){
        variation = malloc(sizeof(comb));
        variation -> mass = checkerBoard;
        variation -> countBlack = countBlack;
        variation -> countWhite = countWhite;
        variation -> next = NULL;
    } else {
        while (variation -> next != NULL) {
            variation = variation -> next;
        }
        variation -> next = malloc(sizeof(comb));
        variation = variation -> next;
        variation -> mass = checkerBoard;
        variation -> countBlack = countBlack;
        variation -> countWhite = countWhite;
        variation -> next = NULL;
    }
}*/
/*
function for output date in struct
@params struct
@return output information on screen
*/
/*
void outputList(comb* variation) {
    comb * outputVariation = variation;

    while (outputVariation != NULL) {
        printf(outputVariation -> mass);
        outputVariation = outputVariation->next;
    }
}
void sort(comb* variation){

}
*/