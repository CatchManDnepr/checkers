#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

void inicilizateBoard(int** checkerDesk);
void outputOnScreen(int** mass);
bool move(int* i, int* j, int ** mass, int side, bool needFight);
bool win(int** checkerDesk);
bool canFightMen(int i, int j, int ** mass, int side);
bool fight(int* placeI, int* placeJ, int ** mass, int side);
void madeKing(int** mass);
int input();
bool check(char str[30]);
bool isInt(char str[30]);
void inputPlace(int side, int** mass, int* i, int* j);
bool canFight(int ** mass, int side);

typedef struct{
    int countWhite;
    int countBlack;
    int** mass;
    struct combination * next;
    struct combination * prev;
}comb;

void virtualPlayer(int** mass, int side);
void push(comb* oldVariation, int** checkerBoard);
void outputList(comb* variation);
